$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "fade",
    controlsContainer: $(".custom-controls-container"),
    customDirectionNav: $(".custom-navigation a")
  });
  if($('.flexslider li').length) {
    $('.link-scroll').click(function() {
      var offset = 0;

      $('html, body').animate({
          scrollTop: $("#benefits").offset().top + offset
      }, 1000);
    });
  }
  $('select').select2();
});